import smtplib
from email.mime.text import MIMEText

# Configuration du serveur SMTP
smtp_server = 'mail.valoria.org'
smtp_port = 587
smtp_username = 'ilove@valoria.org'
smtp_password = 'arandompassword'

# Destinataire de l'email
recipient_email = 'password@valoria.org'

# Création du message
message = MIMEText('Ceci est un test. Aucun mot de passe n\'a été volé.')
message['Subject'] = 'Test de recup de Mot de Passe'
message['From'] = smtp_username
message['To'] = recipient_email

# Envoi du message
try:
    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.starttls()
        server.login(smtp_username, smtp_password)
        server.sendmail(smtp_username, recipient_email, message.as_string())
    print("Email envoyé avec succès.")
except Exception as e:
    print(f"Erreur lors de l'envoi de l'email: {e}")
